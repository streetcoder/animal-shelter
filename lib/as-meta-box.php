<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign


$owners = array();
$p_owners = get_posts( array(
'post_type'      => 'petowners',
'numberposts'    => -1) );

if ($p_owners) {
foreach ($p_owners as $po) {$owners[$po->ID] = $po->post_title;}
}
//$owners_tmp = array_push($owners, "Select");
//$owners_tmp = array_unshift($owners, "Select");

$age = array("Select age ","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30");

$prefix = 'as_';

global $meta_boxes;

$meta_boxes = array();

// 1st meta box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pet',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Pet Information',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'pets' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
    //"options" => $options_pixels);    
	// List of meta fields
	'fields' => array(

        // Pet owner
        array(
            'name'        => 'Pet Owner',
            'id'        => "{$prefix}pet_owner",
            'type'        => 'select',
            'options'    => $owners,
            'multiple'    => false,
            'std'        => array( 'Select' ),
            'desc'        => 'Select which Pet Owner this Pet belongs to'
        ),
        
        // Sex
        array(
            'name'        => 'Sex',
            'id'        => "{$prefix}sex",
            'type'        => 'radio',
            'options'    => array(
                'Male'            => 'Male',
                'Female'          => 'Female'
            ),
            'std'        => 'Male',
        ),

		// Breed
		array(
			'name'		=> 'Breed',
			'id'		=> $prefix . 'breed',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
        
        // Age
        array(
            'name'       => 'Age',
            'id'         => "{$prefix}age",
            'type'       => 'select',
            'options'    => $age,
            'multiple'   => false,
            'std'        => array( '' ),
            'desc'       => 'Enter with number how many years old the pet is'
        ),
        
        // Description
        array(
            'name'    => 'Description',
            'id'    => "{$prefix}desc",
            'type'    => 'wysiwyg',
            'std'    => "",
            'desc'    => ''
        ),
        
        // PLUPLOAD IMAGE UPLOAD (WP 3.3+)
        array(
            'name'    => 'Upload Images',
            'desc'    => 'Upload your pet images (multiple images could be uploaded)',
            'id'    => "{$prefix}image",
            'type'    => 'plupload_image',
            'max_file_uploads' => 3,
        ),    
                
        
	),
	'validation' => array(
		'rules' => array(
			// optionally make post/page title required
			'post_title' => array(
				'required' => true
			),
			$prefix . 'pet_owner' => array(
				'required' => true
			),
		),
		// optional override of default jquery.validate messages
		'messages' => array(
			$prefix . 'pet_owner' => array(
				'required' => 'Pet Owner is required'
			),
		)
	)
);


$meta_boxes[] = array(
    // Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'petOwner',

    // Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => 'Pet Owner Details',

    // Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array( 'petowners' ),

    // Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',

    // Order of meta box: high (default), low. Optional.
    'priority' => 'high',
    //"options" => $options_pixels);    
    // List of meta fields
    'fields' => array(

            // TEXT
        array(
            'name'        => 'Site Number',
            'id'          =>  $prefix .'site_number',                     // $prefix .'site_number', 
            'clone'       => false,
            'type'        => 'text',
            'std'         => ''
        ),
        
        // Address
        array(
            'name'        => 'Address',
            'desc'        => "Enter the address of this site",
            'id'          => $prefix . 'address',
            'type'        => 'textarea',
            'std'         => "",
            'cols'        => "40",
            'rows'        => "8"
        ),       
        
        // City
        array(
            'name'        => 'City',
            'id'          => $prefix . 'city',
            'clone'       => false,
            'type'        => 'text',
            'std'         => ''
        ),
        
        // State
        array(
            'name'        => 'State',
            'id'          => $prefix . 'state',
            'clone'       => false,
            'type'        => 'text',
            'std'         => ''
        ),
        
        // Zip
        array(
            'name'        => 'Zip',
            'id'          => $prefix . 'zip',
            'clone'       => false,
            'type'        => 'text',        
            'std'         => ''
        ),        
        
        // Phone
        array(
            'name'        => 'Phone',
            'id'          => $prefix . 'phone',
            'clone'       => false,
            'type'        => 'text',
            'std'         => ''
        ),        
        
        // Email
        array(
            'name'        => 'Email',
            'id'          => $prefix . 'email',
            'clone'       => false,
            'type'        => 'text',
            'std'         => ''
        ),
        
        // Facebook
        array(
            'name'        => 'Facebook',            
            'id'          => $prefix . 'fb',
            'clone'       => false,
            'type'        => 'text',
            'std'         => '',
            'desc'        => 'Enter your Facebook links'
        ),
        
        // Twitter
        array(
            'name'        => 'Twitter',
            'id'          => $prefix . 'twitter',
            'clone'       => false,
            'type'        => 'text',
            'std'         => '',
            'desc'        => 'Enter your Twitter ID'
        ),
        
        // Bio
        array(
            'name'       => 'Bio',
            'id'         => "{$prefix}bio",
            'type'       => 'wysiwyg',
            'std'        => "",
            'desc'       => ''
        ),
        
        
    ),
    'validation' => array(
        'rules'  => array(
            // optionally make post/page title required
            'post_title'   => array(
                'required' => true
            ),
        
        ),

    )
);


/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function as_register_meta_boxes()
{
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'RW_Meta_Box' ) )
	{
		foreach ( $meta_boxes as $meta_box )
		{
			new RW_Meta_Box( $meta_box );
		}
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'as_register_meta_boxes' );