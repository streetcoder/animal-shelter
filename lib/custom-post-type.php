

<?php
    /*
* Register Custom Post type for pet
* Register Custom Texonomy for pet
* Post Type: pets
* Custom Texonomy: pet-type
*/

function pet_init_func() 
{
  $labels = array(
    'name' => _x('Manage Pets', 'post type general name'),
    'singular_name' => _x('singular Pets', 'post type singular name'),
    'add_new' => _x('Add New Pet', 'Pets'),
    'add_new_item' => __('Add Pet Name'),
    'edit_item' => __('Edit Pet'),
    'new_item' => __('New Pet'),
    'all_items' => __('Manage Pets'),
    'view_item' => __('View Pet'),
    'search_items' => __('Search Pet'),
    'not_found' =>  __('No Pet found'),
    'not_found_in_trash' => __('No Pet found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Pets'

  );
  $args = array(
    'labels' => $labels,
    'public'                => true,
    'show_ui'               => true,
    'supports'              => array( 'title', 'editor','thumbnail' ),
    'show_in_nav_menus'     => true,
    '_builtin'              => false,    
    
    //'rewrite' => array( 'slug' => 'pets', 'with_front' => false ),    
    //'taxonomies' => array( 'Pets'),    
    //'capability_type' => 'post',
    'menu_position' => 10,
    //'supports' => array('title','editor','thumbnail')
  ); 
  register_post_type('pets',$args);


  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Pet Category', 'taxonomy general name' ),
    'singular_name' => _x( 'Pet Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Pet Category' ),
    //'popular_items' => __( 'Popular Pet Category' ),
    'all_items' => __( 'All Pet Category' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Pet Category' ), 
    'update_item' => __( 'Update Pet Category' ),
    'add_new_item' => __( 'Add New Pet Category' ),
    'new_item_name' => __( 'New Pet Category' ),
    'separate_items_with_commas' => __( 'Separate Pet Category with commas' ),
    'add_or_remove_items' => __( 'Add or remove Pet Category' ),
    'choose_from_most_used' => __( 'Choose from the most used Pet Category' ),
    'menu_name' => __( 'Pet Category' ),
  ); 

  register_taxonomy('pet-type','pets',array(
  
    /*'public'                => true,
    'show_ui'               => true,
    'show_in_nav_menus'     => true,
    '_builtin'              => false,  
*/  
  
    'hierarchical' => true,
    'labels' => $labels,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'pet-type' ),
    
  ));
    
}

/*
* Register Custom Post type for pet owner
* Register Custom Texonomy for pet owner
* Post Type: pets
* Custom Texonomy: pet-type
*/

function pet_owner_init_func() 
{
  $labels = array(
    'name' => _x('All Pet Owners', 'post type general name'),
    'singular_name' => _x('singular Pet Owners', 'post type singular name'),
    'add_new' => _x('Add New', 'Pet Owner'),
    'add_new_item' => __('Add Owner Name'),
    'edit_item' => __('Edit Pet Owner'),
    'new_item' => __('New Pet Owner'),
    'all_items' => __('All Pet Owners'),
    'view_item' => __('View Pet Owner'),
    'search_items' => __('Search Pet Owner'),
    'not_found' =>  __('No Pet Owner found'),
    'not_found_in_trash' => __('No Pet Owner found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Pet Owners'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => 'pet-owner', 'with_front' => false ),    
    //'rewrite' => true,
    'taxonomies' => array( 'Pet Owner'),    
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,        
    'menu_position' => 10,
    'supports' => array('title','thumbnail')
  ); 
  register_post_type('petowners',$args);
    
}
?>