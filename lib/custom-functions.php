<?php


/*
*  Change default title placeholder of custom post types
*/

function change_default_title( $title ){
     $screen = get_current_screen();
 
     if  ( 'pets' == $screen->post_type ) {
          $title = 'enter pet name';
     }
     
     if  ( 'petowners' == $screen->post_type ) {
          $title = 'enter pet owner name';
     }     
 
     return $title;
}


/*
* Funtion for Pagination
*/
 
function pagination($pages = '', $range = 4)
{ 
     //$paginate ='';
     $showitems = ($range * 2)+1; 
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }  
 
     if(1 != $pages)
     {
         $paginate .= "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) $paginate .= "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) $paginate .= "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 $paginate .= ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) $paginate .= "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>"; 
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $paginate .= "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         $paginate .= "</div>\n";
     }
     
     return $paginate;     

}

/*
* Custom Search Options
* Search form with dropdown of custom texonomies: pet-type
* Search form 
*/


function petSearch(){
    ?>
<div class="as-search">
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
    <!--<div class="alignleft">-->
    <?php //wp_dropdown_categories( 'show_option_all=Select...' ); ?>
    <select name="taxonomy">
    <option value="0">Select Pet Type</option>
    <?php dropdown_search('pet-type', 'number=0&order=asc'); ?>
    </select>
    <!--</div>-->
    <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" class="searchinput" />
    <input type="submit" id="searchsubmit" value="Search" class="btn" />
    </form>    
</div>    
    <?php
    
}
?>