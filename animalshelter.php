<?php

/**
 * @package Animal Shelter
 * @author Shimul
 * @version 1.0.1
 */
/*
Plugin Name: Animal Shelter
Plugin URI: http://www.streetcoder.net/
Description: Animal Shelter Plugins
Author: Shimul
Version: 1.0.1
Author URI: http://www.streetcoder.net/
*/

define('IMGCROP', plugins_url().'/animalshelter/lib/timthumb.php' );
define('imgurl', plugins_url().'/animalshelter/images' );
require_once(dirname(__FILE__) . '/lib/meta-box/meta-box.php');
require_once(dirname(__FILE__) . '/lib/as-meta-box.php');
require_once(dirname(__FILE__) . '/lib/search-custom-texonomy.php');
require_once(dirname(__FILE__) . '/lib/custom-functions.php');


/*
* Register Custom Post type for pet
* Register Custom Texonomy for pet
* Post Type: pets
* Custom Texonomy: pet-type
*/

function pet_init_func() 
{
  $labels = array(
    'name' => _x('Manage Pets', 'post type general name'),
    'singular_name' => _x('singular Pets', 'post type singular name'),
    'add_new' => _x('Add New Pet', 'Pets'),
    'add_new_item' => __('Add Pet Name'),
    'edit_item' => __('Edit Pet'),
    'new_item' => __('New Pet'),
    'all_items' => __('Manage Pets'),
    'view_item' => __('View Pet'),
    'search_items' => __('Search Pet'),
    'not_found' =>  __('No Pet found'),
    'not_found_in_trash' => __('No Pet found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Pets'

  );
  $args = array(
    'labels' => $labels,
    'public'                => true,
    'show_ui'               => true,
    'supports'              => array( 'title', 'thumbnail' ),
    'show_in_nav_menus'     => true,
    '_builtin'              => false,    
    
    //'rewrite' => array( 'slug' => 'pets', 'with_front' => false ),    
    //'taxonomies' => array( 'Pets'),    
    //'capability_type' => 'post',
    'menu_position' => 10,
    //'supports' => array('title','editor','thumbnail')
  ); 
  register_post_type('pets',$args);


  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Pet Category', 'taxonomy general name' ),
    'singular_name' => _x( 'Pet Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Pet Category' ),
    //'popular_items' => __( 'Popular Pet Category' ),
    'all_items' => __( 'All Pet Category' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Pet Category' ), 
    'update_item' => __( 'Update Pet Category' ),
    'add_new_item' => __( 'Add New Pet Category' ),
    'new_item_name' => __( 'New Pet Category' ),
    'separate_items_with_commas' => __( 'Separate Pet Category with commas' ),
    'add_or_remove_items' => __( 'Add or remove Pet Category' ),
    'choose_from_most_used' => __( 'Choose from the most used Pet Category' ),
    'menu_name' => __( 'Pet Category' ),
  ); 

  register_taxonomy('pet-type','pets',array(
  
    /*'public'                => true,
    'show_ui'               => true,
    'show_in_nav_menus'     => true,
    '_builtin'              => false,  
*/  
  
    'hierarchical' => true,
    'labels' => $labels,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'pet-type' ),
    
  ));
    
}

/*
* Register Custom Post type for pet owner
* Register Custom Texonomy for pet owner
* Post Type: pets
* Custom Texonomy: pet-type
*/

function pet_owner_init_func() 
{
  $labels = array(
    'name' => _x('All Pet Owners', 'post type general name'),
    'singular_name' => _x('singular Pet Owners', 'post type singular name'),
    'add_new' => _x('Add New', 'Pet Owner'),
    'add_new_item' => __('Add Owner Name'),
    'edit_item' => __('Edit Pet Owner'),
    'new_item' => __('New Pet Owner'),
    'all_items' => __('All Pet Owners'),
    'view_item' => __('View Pet Owner'),
    'search_items' => __('Search Pet Owner'),
    'not_found' =>  __('No Pet Owner found'),
    'not_found_in_trash' => __('No Pet Owner found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Pet Owners'

  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true, 
    'query_var' => true,
    'rewrite' => array( 'slug' => 'pet-owner', 'with_front' => false ),    
    //'rewrite' => true,
    'taxonomies' => array( 'Pet Owner'),    
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,        
    'menu_position' => 10,
    'supports' => array('title', 'thumbnail')
  ); 
  register_post_type('petowners',$args);
    
}


/*
*  function for display list of all pet thumbnails and pet name
*  All pet will be displayed by shortcode
*/

function animal_listing_func(){
    $str_result = '';
    
    $thumb_width = get_option('$thumb_width_op');
    $thumb_height = get_option('$thumb_height_op');

    if($thumb_width !=''){
        $img_width = $thumb_width;
    }
    else{
        $img_width = 100;
    }
    
    if($thumb_height !=''){
        $img_height = $thumb_height;
    }
    else{
        $img_height = 100;
    }
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $wp_query = new WP_Query(array(
      'post_type' => 'pets',
      'post_status' => 'publish',
      'posts_per_page' => 20,
      'paged' => $paged  
    ));
    
    if($wp_query->have_posts()):
      $str_result .= petSearch();
      $str_result .= pagination($wp_query->max_num_pages);
      $str_result .= '<div class="as-container clearfix">';
      while ( $wp_query->have_posts() ) : $wp_query->the_post();
        $id = $wp_query->post->ID;
        $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );        
        $str_result .= '<div class="as-pet-thumb">' ;        
        $str_result .= '<div class="as-pet-title clearfix"><a href="'.get_permalink( $id ).'">' . $wp_query->post->post_title . '</a></div>';         
        $str_result .= '<a href="'.get_permalink( $id ).'">' . '<img src="'.IMGCROP.'?src='.$img_src[0].'&h='.$img_height.'&w='.$img_width.'" alt="'.$wp_query->post->post_title.'" title="'.$wp_query->post->post_title.'"  />' . '</a>'; 
        $str_result .= '</div>';

      endwhile;
      $str_result .= '</div>';
      $str_result .= pagination($wp_query->max_num_pages);

    endif;
    
    wp_reset_postdata();
    
    return $str_result;

  }
  
  
/*
* function for pet details
*/

function animal_details_func($content){
//$content = '';
$img_width = 290;
$img_height = 290;    
    
$wpq = new WP_Query(array(
  'post_type' => 'pets',
  'post_status' => 'publish',
  'posts_per_page' => -1
));

if($wpq->have_posts()):

  while ( $wpq->have_posts() ) : $wpq->the_post();
      
    $id = $wpq->post->ID;
/*    $owner_id = get_post_meta($id, 'as_pet_owner', true);
    $owner = get_post_meta($owner_id, 'as_pet_owner', true);*/
    
    $owner_id = get_post_meta($id, 'as_pet_owner', true);
    $page_data = get_page( $owner_id );
    $owner = $page_data->post_title;    
    $owner_phone = get_post_meta($owner_id,'as_phone',true);
	$owner_email=get_post_meta($owner_id,'as_email',true);
	$owner_address=get_post_meta($owner_id,'as_address',true);
    $owner_city=get_post_meta($owner_id,'as_city',true);
	
    $sex = get_post_meta($id, 'as_sex', true);
    $breed = get_post_meta($id, 'as_breed', true);
    $age = get_post_meta($id, 'as_age', true);
    $fb = get_post_meta($owner_id, 'as_fb', true);
    $twitter = get_post_meta($owner_id, 'as_twitter', true);
    $description = get_post_meta($id, 'as_desc', true);
    $image = get_post_meta($id, '_thumbnail_id', true);
    $img_src = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
    
	global $wpdb;
    $images = get_post_meta( $id, 'as_image', false );
    $images = implode( ',' , $images );
    // Re-arrange images with 'menu_order'
                $images = $wpdb->get_col( "
                SELECT ID FROM {$wpdb->posts}
                WHERE post_type = 'attachment'
                AND ID in ({$images})
                ORDER BY menu_order ASC
                " );
    
	
	if( is_single($id) ) {

        ?>
        
        <div class="as-pet-details-wrap clearfix">

            <div class="as-pet-detail-thumb">
            <?php echo '<img src="'.IMGCROP.'?src='.$img_src[0].'&h='.$img_height.'&w='.$img_width.'" alt="'.$wpq->post->post_title.'" title="'.$wpq->post->post_title.'"  />'; ?>
               
               <div class="as-pet-detail-sub-thumb">
				 <?php 
				foreach ( $images as $attach_image )
						 {
						 // Get image's source based on size, can be 'thumbnail', 'medium', 'large', 'full' or registed post thumbnails sizes
						 $src = wp_get_attachment_image_src( $attach_image, 'full' );
						 $src = $src[0];
						
						 // Show image
		 				echo '<img src="'.IMGCROP.'?src='.$src.'&h=100&w=100" alt="'.$wpq->post->post_title.'" title="'.$wpq->post->post_title.'"  />'; 
            			 }
                 ?>
               </div>
            </div>
            
            <div class="as-pet-details">
                
                <div class="as-pet-info">
                    <strong>Pet Name :</strong> <?php echo $wpq->post->post_title; ?>
                </div>
                <div class="as-pet-info">
                    <strong>Sex :</strong> <?php echo $sex; ?>
                </div>
                
                
                <div class="as-pet-info">
                    <strong>Age :</strong> <?php echo $age; ?>
                </div>                
                
                <div class="as-pet-info">
                    <strong>Description :</strong> <?php echo $description; ?>
                </div>  
               <strong>To adopt <?php the_title();?> use the following information below.</strong>
                
                <div class="as-pet-info">
                    <strong> Owner Name :</strong>  <?php echo $owner; ?>
                </div>
                <div class="as-pet-info">
                    <strong> Owner Phone :</strong>  <?php echo $owner_phone; ?>
                </div>
                <div class="as-pet-info">
                    <strong> Owner Email :</strong>  <?php echo '<a href="mailto:'.$owner_email.'?Subject=I am interested in adopting '.$wpq->post->post_title.'">'.$owner_email.'</a>' ;?>
                </div>
                <div class="as-pet-info">
                    <strong> Owner Address :</strong>  <?php echo $owner_address;  ?>
                   
                </div>
                
                <br />
                
            <div class="as-pet-details-sbm clearfix">
           
                <div class="as-pet-details-icon">
                    <a href="<?php echo $fb; ?>" target="_blank"><img src="<?php echo imgurl.'/facebook.png'; ?>" alt="" /></a>        
                </div>
            
                <div class="as-pet-details-icon">
                    <a href="<?php echo $twitter; ?>" target="_blank"><img src="<?php echo imgurl.'/twitter.png'; ?>" alt="" /></a>        
                </div>
                
                <div class="as-pet-details-icon">
                    <a href="<?php bloginfo('url'); ?>/feed/?post_type=pets" target="_blank"><img src="<?php echo imgurl.'/feed.png'; ?>" alt="" /></a>        
                </div>                

            </div>                 
                
            </div>  
            
            <div class="email-to-friend clearfix">
                
            <?php
                $sub_utf8 = "Animal Shelter";

                $Subject="=?UTF-8?B?".base64_encode($sub_utf8)."?=\n";
                $youremail = trim(stripslashes($_POST['yemail'])); 
                //$youremail = "sraboni_apu2@yahoo.com"; 
                $friendemail = trim(stripslashes($_POST['femail'])); 
                //$message = trim(stripslashes($_POST['message'])); 
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                $headers .= 'From: '.$youremail.' <'.$youremail.'>' . "\r\n" . 'Reply-To: ' . $youremail;
                //$link = '<a href="#">'.$wpq->post->post_title.'</a>';
                $link = '<a href="'.get_page_link($id).'">'.$wpq->post->post_title.'</a>';

                $Body = "";    
                $Body .= '
                <html>
                <head>
                  <title>'.$Subject.'</title>
                </head>
                <body>
                <br /><br />Hi '.$friendemail.',<br />

                '.$youremail.' reffered you this link: '.$link.'<br />
                
                Like it!
                </body>
                </html>
                ';

                $success = mail($friendemail, $Subject, $Body, $headers);
                if($success){
                    echo '<h2>Your link sent to your firend successfully !</h2>';
                }
            ?>                
                
                <form action="" method="post">
                            <label>Your email:</label> <br />
                            <input name="yemail" type="text" /><br />
                                            
                            <label>Friend's email:</label><br />
                            <input name="femail" type="text" /><br />
                            <input type="submit" name="submit" value="Tell your friend" />                            
                </form>
            </div>          

<?php

if($owner_city !=''){
    
    echo '<iframe width="600" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q='.$owner_city.'&amp;ie=UTF8&amp;hq=&amp;hnear='.$owner_city.'&amp;z=11&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q='.$owner_city.'&amp;ie=UTF8&amp;hq=&amp;hnear='.$owner_city.'&amp;z=11" style="color:#0000FF;text-align:left">View Larger Map</a></small>';
    
}
?>            
 
 
        </div>
        <?php            
            
    }
    
  endwhile;

endif;

wp_reset_postdata();

return $content;
}  

  
/*  
* Plugins options
* Options for image width and height
*/
  
function settings(){

$thumb_width = get_option('$thumb_width_op');
$thumb_height = get_option('$thumb_height_op');

if(isset($_POST['Submit']))  {

        $thumb_width = $_POST["as_width"];
        update_option( '$thumb_width_op', $thumb_width );
       
        $thumb_height = $_POST["as_height"];
        update_option( '$thumb_height_op', $thumb_height );
        
?>
<div class="updated"><p><strong><?php _e('Options updated.', 'streetcoder' ); ?></strong></p></div>
<?php  
}  
?>

<div class="wrap">

<div class="icon32" id="icon-options-general"><br></div>
<form method="post" name="options" target="_self">
<h2>Pet Options</h2>
<table width="100%" cellpadding="10" class="form-table">
<tr>
<td align="left" scope="row">
<label for="as_width">Image Width</label>
<input name="as_width" value="<?php echo $thumb_width ?>" />
</td> 
</tr>

<tr>
<td align="left" scope="row">
<label for="as_height">Image Height</label>
<input name="as_height" value="<?php echo $thumb_height ; ?>" />
</td> 
</tr>

</table>
<p class="submit">
<input class='button-primary' type="submit" name="Submit" value="Update" />
</p>
</form>

</div>
<?php
}

/*
* custom feed 
*/
function add_cpts_to_rss_feed( $args ) {
  if ( isset( $args['feed'] ) && !isset( $args['post_type'] ) )
    $args['post_type'] = array('post', 'pets');
  return $args;
}

add_filter( 'request', 'add_cpts_to_rss_feed' );

/*function myfeed_request($qv) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array('post', 'pets');
    return $qv;
}
add_filter('request', 'myfeed_request');*/

/*
* Plugins stylsheet to display all pets in frontend as well as details page of individual pet
*/

if( !is_admin()){
   wp_enqueue_style('animalshelter',plugins_url().'/animalshelter/css/animal-shelter.css');
}   

/*
* function for as_settings add submenu page  
*/

function as_settings()
{    
 add_submenu_page( 'edit.php?post_type=pets', 'Settings', 'Settings', 'manage_options', 'as-settings', 'settings' ); 
}

add_action('admin_menu', 'as_settings');
add_action('init', 'pet_init_func');
add_action('init', 'pet_owner_init_func');

add_filter( 'enter_title_here', 'change_default_title' );
add_filter('the_content', 'animal_details_func');

add_shortcode('display pets', 'animal_listing_func');

